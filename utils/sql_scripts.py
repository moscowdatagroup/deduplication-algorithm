
unmatched_products_sql = """
                          SELECT    sku, name, 
                                    id_catalog_config,
                                    fk_catalog_brand                           
                          FROM      catalog_config 
                          WHERE     marketplace_parent_sku IS NULL 
                          AND       marketplace_children_skus IS NULL 
                          AND       status = 'active'"""

matched_products_sql = """
                          SELECT    sku, name 
                          FROM      catalog_config 
                          WHERE     marketplace_parent_sku IS NOT NULL 
                          AND       marketplace_children_skus IS NULL 
                          AND       status = 'active'"""

master_products_sql = """
                          SELECT    sku, name 
                          FROM      catalog_config 
                          WHERE     marketplace_parent_sku IS NULL 
                          AND       marketplace_children_skus IS NOT NULL 
                          AND       status = 'active'"""


unmatched_products_sql_extended = """
                          SELECT * 
                          FROM 
                                   (SELECT    sku, name,id_catalog_config 
                                    FROM      catalog_config 
                                    WHERE     marketplace_parent_sku IS NULL 
                                    AND       marketplace_children_skus IS NULL 
                                    AND       status = 'active') as cat_con
                          LEFT OUTER JOIN
                                   (SELECT    cats.fk_catalog_config as fk_catalog_config,
                                              sou.barcode_ean as GTIN
                                    FROM     
                                              (SELECT id_catalog_simple, fk_catalog_config 
                                               FROM   catalog_simple 
                                               WHERE status = 'active' ) as cats 
                                    LEFT OUTER JOIN 
                                              (SELECT fk_catalog_simple, barcode_ean 
                                               FROM catalog_source) as sou 
                                    ON cats.id_catalog_simple = sou.fk_catalog_simple) as merg
                           ON cat_con.id_catalog_config = merg.fk_catalog_config

                                    """

barcodes_sql = """
                  SELECT   fk_catalog_simple,
                           barcode_ean 
                  FROM     catalog_source
               """

catalogs_join = """
                   SELECT   id_catalog_simple,
                            fk_catalog_config 
                   FROM     catalog_simple 
                   WHERE    status = 'active' 
                """

brand_sql =     """
                   SELECT   id_catalog_brand,
                            name as brand_name 
                   FROM     catalog_brand 
                   WHERE    status = 'active' 
                """

colors_sql = """
                   SELECT   color,
                            count(*) as amount 
                   FROM     catalog_config 
                   GROUP BY color
                   ORDER BY amount desc
                """

