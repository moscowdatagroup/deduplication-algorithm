import re
from unidecode import unidecode
import codecs
#from nltk.stem import SnowballStemmer

#stemmer = SnowballStemmer('spanish')

def delete_specials(text):
    return re.sub(ur'\W+',' ', text, flags = re.UNICODE) 

def delete_whitespace(text):
    result = re.sub(ur'\s+',' ', text, flags = re.UNICODE) 
    return result.rstrip().lstrip()

def to_lowercase(text):
    return text.lower()

def transliteration(text):
    return unidecode(text)

def delete_stop_words(text, stopwords):
    return ' '.join([word for word in text.split() if word not in stopwords]) 


def split_text_numbers(text):
    terms = text.split()
    numbers = [item for item in terms if re.search(r'\d', item)]
    words = [item for item in terms if  not re.search(r'\d', item)]
    return (numbers, words)


def delete_brand(text, brand):
    return ' '.join([word for word in text.split() if word not in brand.split()]) 

def extract_dictionary(text, map_dict):
    return [map_dict.ix[word].values[0] for word in text.split() if word in map_dict.index]

def split_text_numbers(text):
    terms = text.split()
    numbers = [item for item in terms if re.search(r'\d', item)]
    words = [item for item in terms if not re.search(r'\d', item)]
    return (numbers, words)

#def string_stemming(text):
#    return ' '.join([stemmer.stem(word) for word in text.split()])

