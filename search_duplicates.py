import argparse
import sys
from datetime import datetime
import ast

import pandas as pd
import numpy as np

from sqlalchemy import create_engine
from utils.sql_scripts import *
from utils.preprocessing import *

from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction.text import TfidfVectorizer

from annoy import AnnoyIndex

import cPickle as pickle

parser = argparse.ArgumentParser(description='Run search duplicates service')
parser.add_argument('--train', action="store_true", default = False, help='This argument will run process in training mode')
args = parser.parse_args()

class SearchDuplicates:

	def __init__(self):

		self.master = None
		self.colors_synonims = None 
		self.gender_synonims = None
		self.stop_words = None
		self.master_refined_index = None
		self.tfv = TfidfVectorizer(min_df=3,  max_features=None, analyzer='char', 
			ngram_range=(3, 5), use_idf=1,smooth_idf=1,sublinear_tf=1)

		self.svd = TruncatedSVD(n_components=800, algorithm='arpack', random_state=None, tol=0.0)
		self.scaler = StandardScaler(copy=True, with_mean=True, with_std=True)

		self.vectorized_names = None
		self.sku_index = None


	def load_dictionaries(self):

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Read dictionaries from files"
		self.colors_synonims = pd.read_csv('dictionaries/colors_map_to_english.txt')
		self.colors_synonims.index = self.colors_synonims['Original']
		self.colors_synonims  = self.colors_synonims.drop('Original',1)

		self.gender_synonims = pd.read_csv('dictionaries/gender_map.txt')
		self.gender_synonims.index = self.gender_synonims['Original']
		self.gender_synonims  = self.gender_synonims.drop('Original',1)

		with codecs.open("dictionaries/common_stopwords.txt", 'r', 'utf-8') as text_file:
			self.stop_words = text_file.read().splitlines()
			self.stop_words = pd.Series(self.stop_words ).apply(transliteration)

	def read_and_merge_data(self, connection_string):

		engine = create_engine(connection_string) 

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Read from database"
		self.master = pd.read_sql(unmatched_products_sql, engine)
		cat_connect = pd.read_sql(catalogs_join, engine )
		brand = pd.read_sql(brand_sql, engine )

		self.load_dictionaries()

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Merge data from database"
		temp = pd.merge(self.master, cat_connect.drop_duplicates('fk_catalog_config'), 
			left_on='id_catalog_config', right_on='fk_catalog_config', how = 'left')
		temp = pd.merge(temp, brand, left_on='fk_catalog_brand', right_on='id_catalog_brand', how = 'left')

		self.master = temp.drop(['id_catalog_config','id_catalog_simple',
			'fk_catalog_config', 'fk_catalog_brand','id_catalog_brand'], axis=1)
	

	def preprocess_data(self, data):

		text_data = data.loc[:,'name']
		processed_brands = data.loc[:,'brand_name']

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Preprocess brand names"
		processed_brands = processed_brands.fillna(' ')
		processed_brands = processed_brands.apply(delete_specials).apply(to_lowercase).apply(transliteration)
		processed_brands = processed_brands.apply(delete_stop_words,stopwords = set(self.stop_words.values))
		processed_brands = processed_brands.apply(delete_whitespace)

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Preprocess sku names"
		processed_texts = text_data.apply(delete_specials).apply(to_lowercase).apply(transliteration)
		processed_texts = processed_texts.apply(delete_stop_words,stopwords = set(self.stop_words.values))
		processed_texts = processed_texts.apply(delete_whitespace)
		
		return (processed_texts, processed_brands)


	def extract_metafeatures(self, data):

		text_data = data
		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Delete brand from SKU name"
		text_data['name'] = pd.Series(np.vectorize(delete_brand)(text_data['name'], text_data['brand_name']))

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Extract colors and gender"
		item_colors = text_data['name'].apply(extract_dictionary, map_dict = self.colors_synonims)
		item_gender = text_data['name'].apply(extract_dictionary, map_dict = self.gender_synonims)

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Delete colors and gender from SKU name"
		text_data['name'] = text_data['name'].apply(delete_stop_words, stopwords = self.colors_synonims.index)
		text_data['name'] = text_data['name'].apply(delete_stop_words, stopwords = self.gender_synonims.index)


		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Extract numbers from SKU description"
		numbers = text_data.loc[:,'name'].apply(split_text_numbers).apply(lambda x: x[0])
		words = pd.Series(text_data.loc[:,'name'].apply(split_text_numbers)).apply(lambda x: x[1])

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Save metafeatures to dataframe"
		text_data['name'] = pd.Series(words.apply(lambda x: ' '.join(x)))
		text_data['numbers'] = numbers
		text_data['color'] = item_colors
		text_data['gender'] = item_gender

		return text_data

	def vectorize(self, name = None, fit = False):

		if fit:

			master_refined_slice = self.master['name'].drop_duplicates()
			self.master_refined_index = master_refined_slice.index

			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Vectorizing whole matrix"
			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Fit TF-IDF"
			self.tfv.fit(master_refined_slice)
			self.vectorized_names  =  self.tfv.transform(master_refined_slice) 

			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Fit SVD and transform"
			self.vectorized_names  = self.svd.fit_transform(self.vectorized_names)

			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Normalize data"
			self.vectorized_names  = self.scaler.fit_transform(self.vectorized_names)

			return None
		else:

			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Vectorize name"
			transfomed_name = self.tfv.transform(name) 
			transfomed_name = self.svd.transform(transfomed_name)
			transfomed_name = self.scaler.transform(transfomed_name)

			return transfomed_name

	def train_index(self):


		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Creating index for master vectors"
		self.sku_index = AnnoyIndex(self.vectorized_names.shape[1])
		
		for i in xrange(len(self.vectorized_names)):
			self.sku_index.add_item(i, self.vectorized_names[i])

		self.sku_index.build(10) # 10 trees for ANN


	def search_item(self, query_item = None, query_vector = None):

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Finding items"
		nn_res = self.sku_index.get_nns_by_vector(query_vector, 20, include_distances = True)

		# dump preprocessed duplicates match
		dumb_duplicates = self.master.loc[self.master['name'] == query_item['name']].index

		nearest = pd.DataFrame({'distance':nn_res[1],'ids':nn_res[0]})

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Filtering by distance"
		nearest = nearest[nearest['distance'] <= 0.9]

		if len(nearest) or len(dumb_duplicates):

			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Dumb duplicates found: ", len(dumb_duplicates)
			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Nearest candidates found: ", len(nearest)
			print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Filtering started"


			valid_list = [] 
			
			i = 1

			# BADCODED STUFF AGAIN
			# merge potential copies indexes with flags: 1 for nearest neightbours, 0 for dumb match
			merged = zip(dumb_duplicates, np.zeros(len(dumb_duplicates), dtype=np.int)) + zip(nearest['ids'].values, np.ones(len(nearest['ids']), dtype=np.int))

			for copy in merged:

				if copy[1] == 1:
					# select from other index
					copy_row = self.master.ix[self.master_refined_index[copy[0]]]
				else:
					# select from original index
					copy_row = self.master.ix[copy[0]]
				
				print "*"*40
				print "Candidate: ", i
				print "*"*40
				print copy_row


				# SOME HARDCODED FILTERING. 


				if len(query_item['color']) and len(ast.literal_eval(copy_row['color']))  and len(set(query_item['color'])&set(ast.literal_eval(copy_row['color']))) == 0:
					print "Color failed"
					# if original has color and copy has color and their intersection = 0 -> item not a copy
					continue
				
				if len(query_item['gender']) and len(ast.literal_eval(copy_row['gender']))  and len(set(query_item['gender'])&set(ast.literal_eval(copy_row['gender']))) == 0:
					print "Gender failed"
					# if original has gender and copy has gender and their intersection = 0 -> item not a copy
					continue
				
				if len(query_item['numbers']) and len(ast.literal_eval(copy_row['numbers'])) and len(set(query_item['numbers'])&set(ast.literal_eval(copy_row['numbers']))) == 0:
					print "Number failed"
					# if original has numbers and copy has numbers and their intersection = 0 -> item not a copy
					continue
				
				if len(query_item['brand_name']) and len(copy_row['brand_name']) and query_item['brand_name']!=copy_row['brand_name']:
					print "Brand name failed"
					# if original has numbers and copy has numbers and their intersection = 0 -> item not a copy
					continue
				i+=1

				if copy[1] == 1:
					# select from other index
					valid_list.append(self.master_refined_index[copy[0]])
				else:
					# select from original index
					valid_list.append(copy[0])

				
			
			if len(valid_list):

				print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Duplicates found: ", len(valid_list)

				return self.master.ix[valid_list]

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "No candidates"
		return None

	
	def train(self, connection_string):

		self.read_and_merge_data(connection_string)
		self.master.loc[:,'name'], self.master.loc[:,'brand_name'] = self.preprocess_data(self.master)
		self.master = self.extract_metafeatures(self.master)
		self.vectorize(fit=True)
		self.train_index()
		self.save()
		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Training completed"

	def save(self):

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Saving model"

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Saving index"
		self.sku_index.save('data/sku_vectorized_index.ann')

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Saving master"
		self.master.to_csv('data/master.csv', index=False)

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Saving models"
		with open('data/tfidf.pkl','wb') as fp:
			pickle.dump(self.tfv, fp)

		with open('data/svd.pkl','wb') as fp:
			pickle.dump(self.svd, fp)

		with open('data/scaler.pkl','wb') as fp:
			pickle.dump(self.scaler, fp)

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Saving master index"
		with open('data/master_index.pkl','wb') as fp:
			pickle.dump(self.master_refined_index , fp)

	def load(self):

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Loading model"

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Loading index"
		self.sku_index = AnnoyIndex(800)
		self.sku_index.load('data/sku_vectorized_index.ann')

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Loading master"
		self.master = pd.read_csv('data/master.csv')

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Loading models"
		with open('data/tfidf.pkl','rb') as fp:
			self.tfv = pickle.load(fp)

		with open('data/svd.pkl','rb') as fp:
			self.svd = pickle.load(fp)

		with open('data/scaler.pkl','rb') as fp:
			self.scaler = pickle.load(fp)

		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Loading master index"
		
		with open('data/master_index.pkl','rb') as fp:
			self.master_refined_index = pickle.load(fp)

		self.load_dictionaries()


	def find(self, name, brand):

		df = pd.DataFrame({'name':[name], 'brand_name':[brand]})
		df.loc[:,'name'], df.loc[:,'brand_name'] = self.preprocess_data(df)
		df =  self.extract_metafeatures(df)
		df.loc[:,'numbers'] = df.loc[:,'numbers']
		df_vector = self.vectorize(df['name'])


		return self.search_item(df.squeeze(), df_vector[0])



if __name__ == "__main__":

	if args.train:
		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Starting program in training mode"

		connection_string = 'mysql://root:root@127.0.0.1/bob_mx_live?charset=utf8'

		sd = SearchDuplicates()
		sd.train(connection_string)


	else:
		print datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), "Starting program in search mode. Press CTRL+C to terminate"
		
		sd = SearchDuplicates()
		sd.load()

		while True:
			time = datetime.now()
			brand = raw_input('Brand: ')
			sku_name = raw_input('SKU name: ')
			result = sd.find(sku_name, brand)

			if result is not None:
				print result
			print "Time spend: ", datetime.now() - time
			



