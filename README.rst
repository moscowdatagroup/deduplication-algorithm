Duplicates prototype
--------------------

Prototype for SKU duplicates matching algorithm

Install
-------

To install you need simply to do

``python setup.py install``


Help
----

To show help just print 

``python search_duplicates.py --help``


Train mode
----------

To run in train mode you need just to print

``python search_duplicates.py --train``

