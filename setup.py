import os
import sys
from setuptools import setup

def read(fname):
	sys.path.insert(0, os.path.dirname(__file__))
	return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(

      name = "duplicates_matcher",
      version = "0.1",

      description = """Tool for sku duplicates matching""",
      long_description = read('README.rst'),

      author = "Max Kharchenko",
      author_email = "maksim.kharchenko@linio.com",
      install_requires = [
           "pandas",
           "numpy",
           "sqlalchemy",
           "scikit-learn",
           "annoy",
           "unidecode",
           "mysql-python"
      ],

)